import modsim as ms
import pandas
from matplotlib import pyplot


wiki = "https://en.wikipedia.org/wiki/Estimates_of_historical_world_population"

tablas = pandas.read_html(wiki, header=0,index_col=0, decimal='M')

tabla2 = tablas[2]
tabla2.columns = [ 'censo' , 'prb' , 'un' , 'maddison' ,'hyde' , 'tanton' , 'biraben' , 'mj' ,'thomlinson' , 'durand' , 'clark' ]

censo = tabla2.censo/1e9
un = tabla2.un/1e9

#crecimiento_total = censo[2016] - censo[1950]

def step_constante(pop, t, sistema):
    return pop[t] + sistema.crecimiento_anual

def step_proporcional(pop, t, sistema):
    nacimientos = sistema.tasa_nat * pop[t]
    muertes = sistema.tasa_mort * pop[t]
    
    p = pop[t] + nacimientos - muertes
#    print(t+1, p-pop[t])
    return p

def step_proporcional_alpha(pop, t, sistema):
    
    crecimiento_neto = sistema.alpha * pop[t]

    return pop[t] + crecimiento_neto

def step_proporcional_alphas(pop, t, sistema):
    
    if t < 1970:
        crecimiento_neto = sistema.alpha1 * pop[t]
    else:
        crecimiento_neto = sistema.alpha2 * pop[t]

    return pop[t] + crecimiento_neto


def step_cuadratico(pop, t, sistema):
    
    crecimiento_neto = sistema.alpha * pop[t] + sistema.beta * pop[t]**2
    print("Poblacion(%d): %.3f, Crecimiento neto: %.6f" % (t, pop[t], crecimiento_neto))

    return pop[t] + crecimiento_neto


### Simulación
def run_simulation(sistema, step_func):
    resultado = ms.TimeSeries()
    resultado[sistema.t_0] = sistema.p_0
    
    for t in ms.linrange(sistema.t_0, sistema.t_end):
        resultado[t+1] = step_func(resultado, t, sistema)

    return resultado

def plot_resultado(censo, un, resultado, titulo):
    pyplot.clf()
    ms.plot(resultado, '-', label="Modelo")
    ms.plot(censo, ':', label="US censo")
    ms.plot(un, '--', label="UN censo")
    ms.decorate(title=titulo, xlabel='Año', ylabel='Poblacion (miles de millones)')
    ms.savefig("/tmp/poblacion.jpg")



t_0 = ms.get_first_label(censo)
t_end = ms.get_last_label(censo)
p_0 = censo[t_0]
p_end = censo[t_end]

diferencia_tiempo = t_end - t_0
crecimiento_total = p_end - p_0
crecimiento_anual = crecimiento_total / diferencia_tiempo

sistema = ms.System(
        t_0 = t_0,
        t_end = t_end,
        crecimiento_anual = crecimiento_anual,
        p_0 = p_0,
        tasa_nat = 0.027,
        tasa_mort = 0.01)

sistema.alpha = 0.025
sistema.beta = -0.0018

res = run_simulation(sistema, step_cuadratico)
plot_resultado(censo, un, res, "Modelo cuadratico")


# graficando el crecimiento neto
arreglo_pob = ms.linspace(0,15,100)
crecimiento_neto = sistema.alpha * arreglo_pob + sistema.beta * arreglo_pob**2

pyplot.clf()
ms.plot(arreglo_pob, crecimiento_neto)
ms.decorate(xlabel="poblacion", ylabel="crecimiento neto")
ms.savefig("/tmp/crecimientoneto.png")

#sistema.p_0=25
sistema.t_end=2100

res = run_simulation(sistema, step_cuadratico)
plot_resultado(censo, un, res, "Modelo cuadratico")


arreglo_pob = ms.linspace(0,30,12)

crecimiento_neto = sistema.alpha * arreglo_pob + sistema.beta * arreglo_pob**2

pyplot.clf()
for sistema.p_0 in arreglo_pob:
    res = run_simulation(sistema, step_cuadratico)
    ms.plot(res)

ms.decorate(xlabel="año", ylabel="Poblacion")
ms.savefig("/tmp/poblaciones_proyectadas.png")

################ proyecciones

def read_table3(filename = 'https://en.wikipedia.org/w/index.php?title=Estimates_of_historical_world_population&oldid=938127092'):
    tables = pandas.read_html(filename, header=0, index_col=0, decimal='M')
    table3 = tables[3]
    table3.columns = ['censo', 'prb', 'un']
    return table3

tabla3 = read_table3()

print(tabla3)

def plot_projecciones(table):
    censo_proj = table.censo / 1e9
    un_proj = table.un / 1e9

    ms.plot(censo_proj.dropna(), 'b:', label='US')  # b: blue:
    ms.plot(un_proj.dropna(), 'g--', label='UN')       # g-- green--
    ms.savefig('/tmp/grafico_proy.jpg')


sistema = ms.System(
        t_0 = t_0,
        t_end = 2100,
        crecimiento_anual = crecimiento_anual,
        p_0 = p_0,
        alpha = 0.025,
        beta = -0.0018)

#simulamos:
pyplot.clf()
resultado = run_simulation(sistema, step_cuadratico)

plot_resultado(censo, un, resultado, 'Proyecciones')
plot_projecciones(tabla3)

pyplot.clf()
alpha_censo = ms.compute_rel_diff(censo)
ms.plot(alpha_censo, label='US')

alpha_un = ms.compute_rel_diff(un)
ms.plot(alpha_un, label='UN')

ms.decorate(xlabel='Año', label='Tasa de crecimiento neto')
ms.savefig('/tmp/grafico_alpha.jpg')


def recta_alpha(t):
    b = 0.02
    m = -0.00021
    return b + m * (t - 1970)

print(recta_alpha(1975))


# graficamos la recta junto a las graficas anteriores:
ts = ms.linrange(1960, 2200)
print(ts)

alpha_model = ms.TimeSeries(recta_alpha(ts), ts)

print(alpha_model[1975])

ms.plot(alpha_model, color='gray', label='model')
ms.plot(alpha_censo)
ms.plot(alpha_un)
ms.decorate(xlabel='Año', ylabel='tasa de crecimiento neto')
ms.savefig('/tmp/grafico.jpg')

t_0 = 1970
t_end = 2100
p_0 = censo[t_0]

sistema = ms.System(t_0=t_0, 
                t_end=t_end,
                p_0=p_0,
                alpha=0.025, beta=-0.0018,
                funcion_alpha=recta_alpha)

def step_alphadiff(pop, t, sistema):
    net_growth = sistema.funcion_alpha(t) * pop[t]
    return pop[t] + net_growth

resultado_quad = run_simulation(sistema, step_cuadratico)
resultado_alpha = run_simulation(sistema, step_alphadiff)
# y graficamos los resultados
plot_resultado(censo, un, resultado_quad, 'Proyecciones')
ms.plot(resultado_alpha)
plot_projecciones(tabla3)
