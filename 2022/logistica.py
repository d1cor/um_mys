import modsim as ms
import pandas
from matplotlib import pyplot
import numpy as np


def step(x, t, sistema):
    return x[t] * sistema.b * (1-x[t])

### Simulación
def run_simulation(sistema, step_func):
    resultado = ms.TimeSeries()
    resultado[sistema.t_0] = sistema.x_0
    
    for t in ms.linrange(sistema.t_0, sistema.t_end):
        resultado[t+1] = step_func(resultado, t, sistema)

    return resultado

def plot_elongacion(resultado, titulo):
    pyplot.clf()
    pyplot.figure(figsize=(16, 6))
    pyplot.rc('text', usetex=True)
    pyplot.title(titulo, fontsize=25)
    pyplot.xlabel('t')
    pyplot.ylabel('X(t)')
    pyplot.plot(resultado, 'o:r', label="Ec. Logística - Elongación")
    ms.savefig("/tmp/logistica_elong_b=%.2f.jpg" % sistema.b)


def plot_fase(resultado, titulo):
    pyplot.clf()
    pyplot.figure(figsize=(10, 6))
    pyplot.rc('text', usetex=True)

    # graficamos la recta a 45º
    p1 = max(1,1)
    p2 = min(0,0)
    pyplot.plot([p1, p2], [p1, p2], 'b-')

    # graficamos la parabola de la ecuacion normalizada
    xx = ms.linspace(0,1,100)
    yy = [ (sistema.b*x*(1-x)) for x in xx]
    pyplot.plot(xx,yy,'-')

    pyplot.xlabel("X(t)")
    pyplot.ylabel("X(t+1)")
    pyplot.title(titulo, fontsize=25)
    
    # graficar los puntos sobre la parabola
    resultado = resultado.tolist()
    resultado1 = resultado.copy()
    resultado.pop()
    resultado1.pop(0)

    pyplot.plot(resultado,resultado1, 'ro')

#    pyplot.plot(resultado, 'o:r', label="Ec. Logística - Fase")
    ms.savefig("/tmp/logistica_fase_b=%.2f.jpg" % sistema.b)

def x_estable(res, threshold=20, precision=2):
    ultimos = res[-threshold:]
    ultimos = [round(x,precision) for x in ultimos]
    estabilidad = list(set(ultimos))
    return estabilidad



# Feigenbaum---------------------------:

def feigenbaum_v1(s):
    print("Feigenbaum casero")
    sweep_b = ms.SweepSeries()
    b_array = ms.linspace(s.b1,s.b2,int(s.t_end))
    for i in b_array:
        print("Calculando: ", i)
        s.b=i
        resultado = run_simulation(s,step)
        sweep_b[i] = x_estable(resultado,precision=2)
        print(sweep_b[i])


    pyplot.clf()
    pyplot.xlabel("b")
    pyplot.ylabel("Punto de estabilidad")
    pyplot.title(f'Feigenabum diagram | ${s.b1} < b < {s.b2}$  |  $x_0={s.x_0}$')
    for b in b_array:
        for est in sweep_b[b]:
                pyplot.scatter(b, est, color='red', s=0.1)

    ms.savefig("/tmp/feigenbaum_v1.png")

def logistica_math(sistema):
    x_array = [sistema.x_0]
    for i in range(int(sistema.t_end)-1):
        x_array.append(sistema.b * x_array[-1]*(1-x_array[-1]))
#        print("-----", i, sistema.b, x_array[-1])
    #return x_array[int(sistema.t_end-sistema.t_ultimos):]
    return x_array[-int(sistema.t_ultimos):]

def feigenbaum_v2(s):

    b_array = np.linspace(s.b1,s.b2,int(s.t_end))


    x_estable = []
    b_estable = []
    for sistema.b in b_array:
        x_estable.append(logistica_math(s))
        b_estable.append([s.b] * int(s.t_ultimos)) 

#    print(x_estable, len(x_estable))
    x_estable = np.array(x_estable).ravel()
    b_estable = np.array(b_estable).ravel()

    print("sizes: ", len(x_estable), len(b_estable))

    x_estable_acotado = []
    b_estable_acotado = []
    for x,b in zip(x_estable,b_estable):
        if x >s.x1 and x<s.x2:
            x_estable_acotado.append(x)
            b_estable_acotado.append(b)

    #for i in range(len(x_estable)):
    #    print(x_estable[i], b_estable[i])

    #pyplot.style.use('seaborn-whitegrid')
    pyplot.figure(figsize=(10, 6))
    pyplot.xlabel('b')
    pyplot.ylabel('x')
    pyplot.title(f'Feigenabum diagram | ${s.b1} < b < {s.b2}$  |  $x_0={s.x_0}$')
    pyplot.scatter(b_estable_acotado, x_estable_acotado, color='red', s=0.01)
    pyplot.savefig('/tmp/feigenbaum_v2_%.2f-%.2f.png' % (s.b1, s.b2))

def elong_fase():
    sistema = ms.System(
            x_0 = 0.2,
            t_0 = 0,
            t_end = 5000,
            t_ultimos = 10,
            b =3.98
            )

    res = run_simulation(sistema, step)
    title_params = "$x_0 = %.2f, b=%.2f$" % (sistema.x_0, sistema.b)
    plot_elongacion(res, "Ec. Logística - Elongación - %s" % title_params)
    plot_fase(res, "Ec. Logística - Fase - %s" % title_params)

    print(x_estable(res,threshold=10,precision=4))

if __name__ == "__main__":
    
    #elong_fase()

    sistema = ms.System(
            x_0 = 0.2,
            t_0 = 0,
            t_end = 500,
            t_ultimos = 100,
            b =3.5,
            b1=3.83,
            b2=3.87,
            x1=0,
            x2=1
            )

#    feigenbaum_v1(sistema)
    feigenbaum_v2(sistema)


