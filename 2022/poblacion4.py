import modsim as ms
import pandas
from matplotlib import pyplot

"""
ms.State: estado del sistema, cambia con la evolucion de la simul.
    bikeshare: cantidad de bicicletas en cada lado
    poblacional: la poblacion en cada instante de simulación

ms.System:
    bikeshare: cantidad de bicicletas en total, la cantidad de destinos, tasas de partida de cada lado, etc.
    poblacional: tiempo inicial, crecimiento anual, tiempo final, ...
"""


wiki = "https://en.wikipedia.org/wiki/Estimates_of_historical_world_population"

tablas = pandas.read_html(wiki, header=0,index_col=0, decimal='M')

tabla2 = tablas[2]
tabla2.columns = [ 'census' , 'prb' , 'un' , 'maddison' ,'hyde' , 'tanton' , 'biraben' , 'mj' ,'thomlinson' , 'durand' , 'clark' ]

censo = tabla2.census/1e9
un = tabla2.un/1e9

#crecimiento_total = censo[2016] - censo[1950]

def step_constante(pop, t, sistema):
    return pop[t] + sistema.crecimiento_anual

def step_proporcional(pop, t, sistema):
    nacimientos = sistema.tasa_nat * pop[t]
    muertes = sistema.tasa_mort * pop[t]
    
    p = pop[t] + nacimientos - muertes
#    print(t+1, p-pop[t])
    return p

def step_proporcional_alpha(pop, t, sistema):
    
    crecimiento_neto = sistema.alpha * pop[t]

    return pop[t] + crecimiento_neto

def step_proporcional_alphas(pop, t, sistema):
    
    if t < 1970:
        crecimiento_neto = sistema.alpha1 * pop[t]
    else:
        crecimiento_neto = sistema.alpha2 * pop[t]

    return pop[t] + crecimiento_neto


def step_cuadratico(pop, t, sistema):
    
    crecimiento_neto = sistema.alpha * pop[t] + sistema.beta * pop[t]**2
    print("Poblacion(%d): %.3f, Crecimiento neto: %.6f" % (t, pop[t], crecimiento_neto))

    return pop[t] + crecimiento_neto




### Simulación
def run_simulation(sistema, step_func):
    resultado = ms.TimeSeries()
    resultado[sistema.t_0] = sistema.p_0
    
    for t in ms.linrange(sistema.t_0, sistema.t_end):
        resultado[t+1] = step_func(resultado, t, sistema)

    return resultado

def plot_resultado(censo, un, resultado, titulo):
    pyplot.clf()
    ms.plot(resultado, '-', label="Modelo")
    ms.plot(censo, ':', label="US censo")
    ms.plot(un, '--', label="UN censo")
    ms.decorate(title=titulo, xlabel='Año', ylabel='Poblacion (miles de millones)')
    ms.savefig("/tmp/poblacion.jpg")




t_0 = ms.get_first_label(censo)
t_end = ms.get_last_label(censo)
p_0 = censo[t_0]
p_end = censo[t_end]

diferencia_tiempo = t_end - t_0
crecimiento_total = p_end - p_0
crecimiento_anual = crecimiento_total / diferencia_tiempo

sistema = ms.System(
        t_0 = t_0,
        t_end = t_end,
        crecimiento_anual = crecimiento_anual,
        p_0 = p_0,
        tasa_nat = 0.027,
        tasa_mort = 0.01)

sistema.alpha = sistema.tasa_nat - sistema.tasa_mort
print(sistema.alpha)
# < 1970
sistema.alpha1 = sistema.tasa_nat - sistema.tasa_mort

# > 1970
sistema.alpha2 = sistema.tasa_nat - sistema.tasa_mort + 0.0005

#print(sistema.alpha)

sistema.alpha = 0.025
sistema.beta = -0.0018


res = run_simulation(sistema, step_cuadratico)
plot_resultado(censo, un, res, "Modelo cuadratico")

#########
"""
Combinamos las tasas en un nuevo parámetro
    nacimientos = sistema.tasa_nat * pop[t]
    muertes = sistema.tasa_mort * pop[t]

    p = pop[t] + sistema.tasa_nat * pop[t] - sistema.tasa_mort * pop[t]
    p = pop[t] + ( sistema.tasa_nat - sistema.tasa_mort ) * pop[t]
    alpha = sistema.tasa_nat - sistema.tasa_mort
    p_nueva = pop[t] + alpha  * pop[t]
"""

# graficando el crecimiento neto
arreglo_pob = ms.linspace(0,15,100)

crecimiento_neto = sistema.alpha * arreglo_pob + sistema.beta * arreglo_pob**2

pyplot.clf()
ms.plot(arreglo_pob, crecimiento_neto)
ms.decorate(xlabel="poblacion", ylabel="crecimiento neto")
ms.savefig("/tmp/crecimientoneto.png")

"""
0 = p(a + bp)
p = -a/b
0 = -a/b * 0

"""
