import modsim as ms

bikeshare = ms.State(um=10, comedor=2)

print(bikeshare)
print(bikeshare.um)
print(bikeshare.comedor)

bikeshare.um -=1
bikeshare.comedor +=1
print(bikeshare)

def bike_to_comedor(state):
    if state.um==0:
        return
    state.um -=1
    state.comedor +=1

def bike_to_um(state):
    if state.comedor==0:
        return
    state.um +=1
    state.comedor -=1

#redefinimos
bikeshare = ms.State(um=10, comedor=2)

def step(state, p1, p2):
    if ms.flip(p1):
        bike_to_comedor(state)
        print("Moviendo al comedor")


    if ms.flip(p2):
        bike_to_um(state)
        print("Moviendo al um")


#redefinimos
bikeshare = ms.State(um=10, comedor=2)
resultado_um = ms.TimeSeries()
resultado_comedor = ms.TimeSeries()

for i in range(10):
    step(bikeshare, 0.4, 0.6)
    resultado_um[i] = bikeshare.um
    resultado_comedor[i] = bikeshare.comedor

print(resultado_um)
print(resultado_um.mean())

ms.plot(resultado_um, label='UM')
ms.plot(resultado_comedor, label='Comedor')
ms.decorate(title='Bicis UM-comedor', xlabel='Tiempo (hr)', ylabel='Numero de bicis')
ms.savefig('/tmp/bicis.png')

def run_simulation(state, p1, p2, steps):
    """
    Corre la simulación
    llama a step()
    """
    resultado_um = ms.TimeSeries()
    resultado_comedor = ms.TimeSeries()

    for i in range(steps):
        step(state, p1, p2)
        resultado_um[i] = state.um
        resultado_comedor[i] = state.comedor

    return resultado_um, resultado_comedor




# estado inicial:
bikeshare = ms.State(um=10, comedor=2)

r_um, r_comedor = run_simulation(bikeshare, 0.4, 0.6, 100)

print(r_um)
