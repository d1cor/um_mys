import modsim as ms
from matplotlib import pyplot


def bike_to_comedor(state):
    if state.um==0:
        state.um_vacio+=1
        return
    state.um -=1
    state.comedor +=1

def bike_to_um(state):
    if state.comedor==0:
        state.comedor_vacio+=1
        return
    state.um +=1
    state.comedor -=1

def step(state, p1, p2):

    state.reloj +=1

    if ms.flip(p1):
        bike_to_comedor(state)
        #print("Moviendo al comedor")

    if ms.flip(p2):
        bike_to_um(state)
        #print("Moviendo al um")

    if state.t_primer_disconforme != -1:
        return

    if state.um_vacio + state.comedor_vacio > 0:
        # ya tengo un disconforme
        state.t_primer_disconforme = state.reloj




def run_simulation(p1, p2, steps):
    """
    Corre la simulación
    llama a step()
    """
    # estado inicial:
    state = ms.State(um=10, comedor=2, um_vacio=0, comedor_vacio=0, t_primer_disconforme=-1, reloj=0)

    resultado_um = ms.TimeSeries()
    resultado_comedor = ms.TimeSeries()

    for i in range(steps):
        step(state, p1, p2)
        resultado_um[i] = state.um
        resultado_comedor[i] = state.comedor

    return state, resultado_um, resultado_comedor


def plotear(r_um, r_comedor):
    ms.plot(r_um, label='UM')
    ms.plot(r_comedor, label='Comedor')
    ms.decorate(title='Bicis UM-comedor', xlabel='Tiempo (hr)', ylabel='Numero de bicis')
    ms.savefig('/tmp/bicis.png')


bikeshare, r_um, r_comedor = run_simulation(0.4, 0.6, 100)
plotear(r_um, r_comedor)

print("UM vacio: ", bikeshare.um_vacio)
print("Comedor vacio: ", bikeshare.comedor_vacio)
print("Ciclo primer disconforme: ", bikeshare.t_primer_disconforme)

"""
Metrica: calculo para agregar información
Ej: cantidad de disconformes al final de la simulación
Ej: tiempo (ciclos) que transcurre hasta que aparece el primer disconforme.


El modelo usa randoms (ms.flip()) -> modelo estocástico
A diferencia del modelo determinístico

"""
def jugando_con_estocastico():
    estados = []
    for i in range(100):
        estados.append(run_simulation(0.4, 0.6, 100))

    tiempos_disc = 0
    for est in estados:
        tiempos_disc += est[0].t_primer_disconforme

    print("Promedio tiempos disc: ", tiempos_disc/100)

"""
Barrido de parametros (sweeping de parametros)
"""
def sweep_p2(arreglo):
    p1 = 0.5
    p2_array = ms.linspace(0,1,100)
    steps = 100
    sweep_p2 = ms.SweepSeries()
    sweep_p2a = ms.SweepSeries()
    for p2 in arreglo:
        estado = run_simulation(p1, p2, steps)
        print("Probabilidad p2: ", p2)
        sweep_p2[p2] = estado[0].um_vacio
        sweep_p2a[p2] = estado[0].comedor_vacio

    return sweep_p2, sweep_p2a

p2_array = ms.linspace(0,1,100)

s_p2, s_p2a = sweep_p2(p2_array)


pyplot.clf()
ms.plot(s_p2, label='Disconformes UM')
ms.plot(s_p2a, label='Disconformes Comedor')
ms.decorate(title='Bicis UM-comedor', xlabel='p2', ylabel='Numero de disconformes')
ms.savefig('/tmp/bicis.png')





