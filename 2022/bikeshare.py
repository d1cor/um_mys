import modsim as ms

bikeshare = ms.State(um=10, comedor=2)

print(bikeshare)
print(bikeshare.um)
print(bikeshare.comedor)

bikeshare.um -=1
bikeshare.comedor +=1
print(bikeshare)

def bike_to_comedor():
    bikeshare.um -=1
    bikeshare.comedor +=1

def bike_to_um():
    bikeshare.um +=1
    bikeshare.comedor -=1

bike_to_comedor()
bike_to_comedor()
bike_to_comedor()
bike_to_um()
print(bikeshare)

#redefinimos
bikeshare = ms.State(um=10, comedor=2)

def step(p1, p2):
    if ms.flip(p1):
        bike_to_comedor()
        print("Moviendo al comedor")


    if ms.flip(p2):
        bike_to_um()
        print("Moviendo al um")

step(0.8, 0.1)
print(bikeshare)

for i in range(5):
    step(0.6, 0.8)
print(bikeshare)


#redefinimos
bikeshare = ms.State(um=10, comedor=2)
resultado_um = ms.TimeSeries()
resultado_comedor = ms.TimeSeries()

for i in range(10):
    step(0.1, 0.8)
    resultado_um[i] = bikeshare.um
    resultado_comedor[i] = bikeshare.comedor

print(resultado_um)
print(resultado_um.mean())

ms.plot(resultado_um, label='UM')
ms.plot(resultado_comedor, label='Comedor')
ms.decorate(title='Bicis UM-comedor', xlabel='Tiempo (hr)', ylabel='Numero de bicis')
ms.savefig('/tmp/bicis.png')


