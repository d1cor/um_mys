import modsim as ms
import pandas
from matplotlib import pyplot

"""
ms.State: estado del sistema, cambia con la evolucion de la simul.
    bikeshare: cantidad de bicicletas en cada lado
    poblacional: la poblacion en cada instante de simulación

ms.System:
    bikeshare: cantidad de bicicletas en total, la cantidad de destinos, tasas de partida de cada lado, etc.
    poblacional: tiempo inicial, crecimiento anual, tiempo final, ...
"""


wiki = "https://en.wikipedia.org/wiki/Estimates_of_historical_world_population"

tablas = pandas.read_html(wiki, header=0,index_col=0, decimal='M')

tabla2 = tablas[2]
tabla2.columns = [ 'census' , 'prb' , 'un' , 'maddison' ,'hyde' , 'tanton' , 'biraben' , 'mj' ,'thomlinson' , 'durand' , 'clark' ]

censo = tabla2.census/1e9
un = tabla2.un/1e9

#crecimiento_total = censo[2016] - censo[1950]


### Simulación
def run_simulation_cte(sistema):
    resultado = ms.TimeSeries()
    resultado[sistema.t_0] = sistema.p_0
    
    for t in ms.linrange(sistema.t_0, sistema.t_end):
        resultado[t+1] = resultado[t] + sistema.crecimiento_anual

    return resultado

def run_simulation_prop(sistema):
    """
    Crecimiento proporcional
    tasas de natalidad y mortalidad
    """
    resultado = ms.TimeSeries()
    resultado[sistema.t_0] = sistema.p_0
    
    for t in ms.linrange(sistema.t_0, sistema.t_end):
        nacimientos = sistema.tasa_nat * resultado[t]
        muertes = sistema.tasa_mort * resultado[t]
        resultado[t+1] = resultado[t] + nacimientos - muertes

    return resultado




def plot_resultado(censo, un, resultado, titulo):
    pyplot.clf()
    ms.plot(resultado, '-', label="Modelo constante")
    ms.plot(censo, ':', label="US censo")
    ms.plot(un, '--', label="UN censo")
    ms.decorate(title=titulo, xlabel='Año', ylabel='Poblacion (miles de millones)')
    ms.savefig("/tmp/poblacion.jpg")




t_0 = ms.get_first_label(censo)
t_end = ms.get_last_label(censo)
p_0 = censo[t_0]
p_end = censo[t_end]
t_end = 2030

diferencia_tiempo = t_end - t_0
crecimiento_total = p_end - p_0
crecimiento_anual = crecimiento_total / diferencia_tiempo

sistema = ms.System(
        t_0 = t_0,
        t_end = t_end,
        crecimiento_anual = crecimiento_anual,
        p_0 = p_0,
        tasa_nat = 0.027,
        tasa_mort = 0.01)

res = run_simulation_prop(sistema)
plot_resultado(censo, un, res, "Modelo Proporcional")


