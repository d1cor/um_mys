import modsim as ms
import pandas
from matplotlib import pyplot


wiki = "https://en.wikipedia.org/wiki/Estimates_of_historical_world_population"

tablas = pandas.read_html(wiki, header=0,index_col=0, decimal='M')

tabla2 = tablas[2]

tabla2.columns = [ 'census' , 'prb' , 'un' , 'maddison' ,'hyde' , 'tanton' , 'biraben' , 'mj' ,'thomlinson' , 'durand' , 'clark' ]

censo = tabla2.census/1e9
un = tabla2.un/1e9

ms.plot(censo, ':', label="US censo")
ms.plot(un, '--', label="UN censo")
ms.decorate(title='Poblacion mundial', xlabel='Año', ylabel='Poblacion (miles de millones)')
ms.savefig("/tmp/poblacion.jpg")

print(censo[1950])

#crecimiento_total = censo[2016] - censo[1950]

t_0 = ms.get_first_label(censo)
t_0 = 1970 # ajustando el modelo constante

t_end = ms.get_last_label(censo)

p_0 = censo[t_0]
p_end = censo[t_end]

diferencia_tiempo = t_end - t_0
print(diferencia_tiempo)

crecimiento_total = p_end - p_0

crecimiento_anual = crecimiento_total / diferencia_tiempo

print(crecimiento_anual)

### Simulación
"""
Inicia en 1950, termina en 2016
armar una 'maquina' que calcule las poblaciones en cada año
"""
resultado = ms.TimeSeries()
resultado[t_0] = censo[t_0]

for t in ms.linrange(t_0, t_end):
    resultado[t+1] = resultado[t] + crecimiento_anual

pyplot.clf()
ms.plot(resultado, '-', label="Modelo constante")
ms.plot(censo, ':', label="US censo")
ms.plot(un, '--', label="UN censo")
ms.decorate(title='Poblacion mundial', xlabel='Año', ylabel='Poblacion (miles de millones)')
ms.savefig("/tmp/poblacion.jpg")

