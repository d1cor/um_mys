# Changelog
All notable changes to this project will be documented in this file.

### [v0.71] 2022-11-16
- Minor typo fixes
- Fix Lotka-Volterra Equilibrium equation

### [v0.7] 2022-05-11
- Section 3.10.5: Ecuación Logística: add Feigenbaum diagrams generated with python.
- Fix minor type errors
- Fix Figs reference numeration.
- Fix minor equation errors

### [v0.5] 2021-07-14
- Fix minor typo errors
- Fix Figs reference numeration
- Chapter 6: Lotka-Volterra: add mathematical demostration contour equation.

### [v0.4] 2021-05-20
- "Sistema de Bicicletas compartidas" chapter (2) added.
- Sections 2.1-2.5 added.
- "Introducción" chapter TO-DO notes updated
- Fixed minor typo errors

### [v0.3] 2021-05-19
- "Modelo Poblacional" chapter (3) added
- Sections 3.1-3.9 added
- "Introducción" chapter TO-DO notes updated
- Fixed minor typo errors

### [v0.2] 2021-05-06
- Index updated
- Fixed minor errors

### [v0.1] 2020-06-21
- Initial version added
